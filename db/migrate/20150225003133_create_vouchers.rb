class CreateVouchers < ActiveRecord::Migration
  def change
    create_table :vouchers do |t|
      t.integer :user_id
      t.integer :event_id
      t.integer :codigo

      t.timestamps null: false
    end
  end
end
