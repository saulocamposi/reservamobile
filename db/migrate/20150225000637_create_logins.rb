class CreateLogins < ActiveRecord::Migration
  def change
    create_table :logins do |t|
      t.integer :user_id
      t.string :passwd

      t.timestamps null: false
    end
  end
end
