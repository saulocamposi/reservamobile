class CreateEvents < ActiveRecord::Migration
  def change
    create_table :events do |t|
      t.integer :party_id
      t.string :name
      t.string :description
      t.string :photo

      t.timestamps null: false
    end
  end
end
