json.array!(@events) do |event|
  json.extract! event, :id, :party_id, :name, :description, :photo
  json.url event_url(event, format: :json)
end
