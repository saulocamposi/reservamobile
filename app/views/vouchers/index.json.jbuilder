json.array!(@vouchers) do |voucher|
  json.extract! voucher, :id, :user_id, :codigo
  json.url voucher_url(voucher, format: :json)
end
