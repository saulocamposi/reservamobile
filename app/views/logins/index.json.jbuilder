json.array!(@logins) do |login|

  json.extract! login, :id, :user_id, :login, :passwd

  json.url login_url(login, format: :json)
end
